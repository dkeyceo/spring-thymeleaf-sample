
const infoContainer = document.getElementById("print_out");
const btn = document.getElementById("btn");
const tit = document.getElementById("try_hide");
const brr3 = document.getElementById('third');
let submiting = false;

function fetchDriversLicense() {
    const vvod = $('#vvvod').val();
    const vvod2 = $('#vvvod2').val();
    if(submiting) return;
    submiting = true;
    fetch (`http://research.hsc.gov.ua/api/docs-service/documents/drvlics/${vvod}/${vvod2}`)
    /*.then(response => {
        console.log(response);
        /!*console.log(responseJson.categories[1].category);*!/
       /!* Out(response);*!/
    })*/
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                return Promise.reject('something went wrong!')
            }
        }).then(response => {
            console.log(response);
            Out(response);
    })
         .catch(error => console.log('error is', error));
}

// $("#myForm").on('submit',function(){
//
//     const vvod = $('#vvvod').val();
//     const vvod2 = $('#vvvod2').val();
//
//     fetch (`http://research.hsc.gov.ua/api/docs-service/documents/drvlics/${vvod}/${vvod2}`)
//         /*.then(response => {
//             console.log(response);
//             /!*console.log(responseJson.categories[1].category);*!/
//            /!* Out(response);*!/
//         })*/
//         .then(response => {
//         if (response.ok) {
//             return response.json()
//         } else {
//             return Promise.reject('something went wrong!')
//         }
//     })
//         .then(data => console.log(data))
//         .catch(error => console.log('error is', error));
//
//         return false;
//
// });

function Out(data) {
    console.log(data.docType.VALUE);
    console.log(data.country.VALUE);
    console.log(data.sdoc);
    console.log(data.ndoc);
    console.log(data.ddoc);
    console.log(data.dend);
    console.log(data.status.VALUE);
    console.log(data.categories[0].category, data.categories[0].dopen);
    /*console.log(data.categories[1].category, data.categories[1].dopen);*/
    console.log(data.department.VALUE);

    if (data && Object.keys(data).length) {

        brr = document.getElementById('second');
        brr2 = document.getElementById('first');

        $(brr2).hide(1000);
        $(brr).show(1000);

        let html = `<div class="container_out">`;
        
        html += `<div class="rowContainer">
                        <div class="firstDiv">А</div>
                        <div class="secondDiv">Тип документа:</div>
                        <div class="lastDiv">${(data.docType.VALUE) ? data.docType.VALUE : '0'}</div>
                    </div>`
        html += `</div>`
        html += `<div class="rowContainer">
                        <div class="firstDiv">В</div>
                        <div class="secondDiv">Країна:</div>
                        <div class="lastDiv">${(data.country.VALUE) ? data.country.VALUE : '0'}</div>
                    </div>`
        html += `</div>`
        
        html += `<div class="rowContainer">
                        <div class="firstDiv">C</div>
                        <div class="secondDiv">Серія, номер:</div>
                        <div class="lastDiv">${(data.sdoc) ? data.sdoc : '0'} ${(data.ndoc) ? data.ndoc : '0'} </div>
                    </div>`
        html += `</div>`

        html += `<div class="rowContainer">
                        <div class="firstDiv">D</div>
                        <div class="secondDiv">Дата видачі:</div>
                        <div class="lastDiv">${(data.ddoc) ? data.ddoc : '0'}</div>
                    </div>`
        html += `</div>`
        html += `<div class="rowContainer">
                        <div class="firstDiv">E</div>
                        <div class="secondDiv">Дата закінчення дії:</div>
                        <div class="lastDiv">${(data.dend) ? data.dend : '0'}</div>
                    </div>`
        html += `</div>`
        html += `<div class="rowContainer">
                        <div class="firstDiv">F</div>
                        <div class="secondDiv">Статус:</div>
                        <div class="lastDiv">${(data.status.VALUE) ? data.status.VALUE : '0'}</div>
                    </div>`
        html += `</div>`

        for (var i=0; i<data.categories.length; i++) {
        html += `<div class="rowContainer">
                        <div class="firstDiv">G</div>
                        <div class="secondDiv">Категорія, дата видачі:</div>
                        <div class="lastDiv">${(data.categories[i].category) ? data.categories[i].category : '0'},
                        ${(data.categories[i].dopen) ? data.categories[i].dopen : '0'}</div>
                    </div>`
        html += `</div>`
        }
        /*html += `<div class="rowContainer">
                        <div class="firstDiv">H</div>
                        <div class="secondDiv">&nbsp;</div>
                        <div class="lastDiv">${(data.categories[1].category) ? data.categories[1].category : '0'},
                        ${(data.categories[1].dopen) ? data.categories[1].dopen : '0'}</div>
                    </div>`
        html += `</div>`*/
         html += `<div class="rowContainer">
                        <div class="firstDiv">H</div>
                        <div class="secondDiv">Підрозділ:</div>
                        <div class="lastDiv">${(data.department.VALUE) ? data.department.VALUE : '0'}</div>
                    </div>`
        html += `</div>`

        infoContainer.insertAdjacentHTML('beforeend', html);
    }
   setTimeout(() => submiting = false, 1000);


}

function newPage() {
    window.location.assign("http://hsc.gov.ua/test_pv/")
}

const keys = {
    ddoc: {
        field1: `A`,
        field2: `Дата видачі:`
    },
    dend: {
        field1: `B`,
        field2: `Дата закінчення:`
    },
    sdoc: {
        field1: `C`,
        field2: `Серія:`
    },
    ndoc: {
        field1: `C1`,
        field2: `Номер:`
    },
    ['status.VALUE']: {
        field1: `B.1`,
        field2: `Статус:`
    },
    makeYear: {
        field1: `B.2`,
        field2: `Рік випуску:`
    },
    brand: {
        field1: `D.1`,
        field2: `Марка:`
    },
    model: {
        field1: `D2`,
        field2: `Модель:`
    },
    kindBody: {
        field1: `D3`,
        field2: `Тип:`
    },
    vin: {
        field1: `E`,
        field2: `Номер шасі (кузова, рами):`
    },
    totalWeight: {
        field1: `F.1`,
        field2: `Повна маса :`
    },
    ownWeight: {
        field1: `G`,
        field2: `Маса без навантаження :`
    },
    rankCategory: {
        field1: `J`,
        field2: `Категорія :`
    },
    capacity: {
        field1: `P.1`,
        field2: `Об'єм двигуна:`
    },
    fuel: {
        field1: `P.2`,
        field2: `Тип палива:`
    },
    color: {
        field1: `R`,
        field2: `Колір:`
    },
    nseating: {
        field1: `S.1`,
        field2: `Кіль-сть сидячих місць з місцем водія:`
    },
    nstandup: {
        field1: `S.2`,
        field2: `Кіль-сть стоячих місць:`
    },
};

function renderHTML(data) {

    if (data && Object.keys(data).length) {

        brr = document.getElementById('second');
        brr2 = document.getElementById('first');

        $(brr2).hide(1000);
        $(brr).show(1000);

        /* document.getElementById('try_hide' ).style.display = 'block';*/
        /* document.getElementById('try_hide_first').style.display = 'none';*/
        let html = `<div class="container_out">`;

        const keysKeys = Object.keys(keys);
        keysKeys.forEach(key => {
            /*const lastDiv = (item[key]) ? item[key] : '?'*/
            html += `<div class="rowContainer">
                        <div class="firstDiv">${keys[key].field1}</div>
                        <div class="secondDiv">${keys[key].field2}</div>
                        <div class="lastDiv">${(data[key]) ? data[key] : '0'}</div>
                    </div>`
    });

        html += `</div>`;

        infoContainer.insertAdjacentHTML('beforeend', html);
    }
    setTimeout(() => submiting = false, 1000);
}